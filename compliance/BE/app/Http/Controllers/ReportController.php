<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;


class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $report = Report::where('user_id', auth('api')->user()->id)->get();
        return response()->json($report);
    }

    public function update(Request $request, $id){
        $report = Report::where('id', $id)->first();
        $report->update(['report_title' => $request->report_title, 'report_desc' => $request->report_desc]);
        return response()->json(['success' => 'Report updated successfully']);
    }

    public function delete($id){
        Report::destroy($id);
        return response()->json(['success' => 'Report deleted successfully']);
    }

    public function store(Request $request)
    {
       
        $data = [
            'report_title' => $request->report_title,
            'report_desc' => $request->report_desc,
            'user_id' => auth('api')->user()->id
        ];
        
        $report = Report::create($data);
        return response()->json(['success' => 'Report created successfuly!']);
    }
}
