<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function (){
        Route::post('login', [AdminController::class, 'login']);
        Route::post('logout', [AdminController::class, 'logout']);
        Route::get('index', [AdminController::class, 'index']);
        Route::post('me', [AdminController::class, 'me']);
        Route::delete('delete/{id}', [AdminController::class, 'delete']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::post('store', [UserController::class, 'store']);
        Route::post('login', [UserController::class, 'login']);
        Route::post('logout', [UserController::class, 'logout']);
        Route::get('index', [UserController::class, 'index']);
        Route::put('update', [UserController::class, 'update']);
        Route::post('me', [UserController::class, 'me']);
    });

    Route::group(['prefix' => 'report'], function (){
        Route::post('store', [ReportController::class, 'store']);
        Route::get('index', [ReportController::class, 'index']);
        Route::put('update/{id}', [ReportController::class, 'update']);
        Route::delete('delete/{id}', [ReportController::class, 'delete']);
    });
});
