import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import router from './routes'
import store from './store'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

Vue.use(Toast, {
  transition: "Vue-Toastification__fade",
  maxToasts: 4,
  position: "top-right",
  dismissible: true,
  hideProgressBar: true,
  duration: 500
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
